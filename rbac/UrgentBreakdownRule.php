<?php
namespace app\rbac;
use app\models\Breakdown;
use yii\rbac\Rule;
use Yii; 
use yii\db\ActiveRecord;

class UrgentBreakdownRule extends Rule
{
	public $name = 'urgentBreakdownRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			if(isset($_GET['id'])){
			    $checkUser = Breakdown::findOne($user);
			        
			    if($checkUser->statusId == $_GET['id'])
			        return true;
			}
		}
		return false;
	}
}