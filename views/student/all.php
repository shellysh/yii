<?php
//All students

use yii\helpers\Html;
use yii\web\Controller;
use app\models\Student;

$this->title = 'View All';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <table border = "2" width = "50%"> 
        <tr> 
            <th>ID</th> 
            <th>Name</th> 
            <th>Age</th>
        </tr> 
	
        <?php 
		foreach($table as $i){
			echo "<tr>";
			echo "<td>".$i->id."</td>"; 
			echo "<td>".$i->name."</td>";
			echo "<td>".$i->age."</td>";
			echo "</tr>";	
		}
		?>
    </table>

    <code><?= __FILE__ ?></code>
</div>