<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Deal;
use app\models\Lead;

/* @var $this yii\web\View */
/* @var $model app\models\Lead */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Leads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
	<?php  ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>
	
	<?php
	//2016 - 5.a
		echo 'Deal: <br>';
		foreach($model->dealItem as $i){
			echo '<a href="?r=deal/view&id='.$i->id.'">'.$i->name.'</a><br>'; 
		}
	
	?>

</div>