<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Level;
use app\models\Status;

/* @var $this yii\web\View */
/* @var $model app\models\Breakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="breakdown-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php //$form->field($model, 'levelId')->textInput() ?>
	<?= $form->field($model, 'levelId')->
				dropDownList(Level::getLevels()) ?> 

    <?php //$form->field($model, 'statusId')->textInput() ?>
	<?php //$form->field($model, 'statusId')-> dropDownList(Status::getStatuses()) ?>
	
	<?php if(!$model->isNewRecord){ ?>
	<?= $form->field($model, 'statusId')->dropDownList(Status::getStatuses()) ?>
	<?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
