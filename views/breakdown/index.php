<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Level;
use app\models\Status;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BreakdownSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Breakdowns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="breakdown-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Breakdown', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            //'levelId',
			//7
			[ 
				'attribute' => 'levelId',
				'label' => 'Level',
				'format' => 'html',
				'value' => function($model){
					return $model->levelItem->name;
				},
				'filter'=>Html::dropDownList('BreakdownSearch[levelId]', $level, $levels, ['class'=>'form-control']),
			],
            //'statusId',
			[ 
				'attribute' => 'statusId',
				'label' => 'Status',
				'format' => 'html',
				'value' => function($model){
					return $model->statusItem->name;
				},
				'filter'=>Html::dropDownList('BreakdownSearch[statusId]', $status, $statuses, ['class'=>'form-control']),
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
