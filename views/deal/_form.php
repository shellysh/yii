<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\web\JsExpression;
use app\models\Lead;
use app\models\Deal;
//use yii\jui\Autocomplete;

/* @var $this yii\web\View */
/* @var $model app\models\Deal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="deal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php //$form->field($model, 'leadId')->textInput() 
		//$data = Lead::getLeads();
	?>
		
	<?php  //$data = Deal::find()->all();

    /*echo AutoComplete::widget([
    'name' => 'Company',
    'id' => 'leadId',
    'clientOptions' => [
        'source' => $data,
        'autoFill'=>true,
        'minLength'=>'2',
        'select' => new JsExpression("function( event, ui ) {
                $('#user-company').val(ui.item.id);
            }")
        ],
     ]);*/
     ?>
	<?php //2016 - 3.a ?>
	<?= $form->field($model, 'leadId')->
				dropDownList(Lead::getLeads()) ?> 

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
