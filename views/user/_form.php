<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
	
	<?php //3.b
	//שדרוג לתפקיד לא יופיע ביצירת משתמש, אמור להופיע רק בעדכון ובנוסף לא יופיע למי שלא בעל הרשאת אדמין
	//if(!$model->isNewRecord && \Yii::$app->user->can('admin', ['user' =>$model]))
	if(\Yii::$app->user->can('admin', ['user' =>$model])){ ?>
	<?= $form->field($model, 'role')->dropDownList(User::getRoles()) ?>
	<?php } ?>
	
	<?php //= $form->field($model, 'role')->dropDownList(User::getRoles()) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
