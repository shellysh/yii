<?php

use yii\db\Migration;

class m170520_154111_add_column_description extends Migration
{
    public function up()
    {
		$this->addColumn('customers', 'description', $this->text());
    }

    public function down()
    {
        $this->dropColumn('customers', 'description');
    }
}
