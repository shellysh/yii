<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lead`.
 */
class m170719_064744_create_lead_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('lead', [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('lead');
    }
}
