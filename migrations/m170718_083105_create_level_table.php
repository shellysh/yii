<?php

use yii\db\Migration;

/**
 * Handles the creation of table `level`.
 */
class m170718_083105_create_level_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('level', [
            'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('level');
    }
}
