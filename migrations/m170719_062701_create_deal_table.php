<?php

use yii\db\Migration;

/**
 * Handles the creation of table `deal`.
 */
class m170719_062701_create_deal_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('deal', [
            'id' => $this->primaryKey(),
			'leadId' => $this->integer()->notNull(),
			'name' => $this->string()->notNull(),
			'amount' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('deal');
    }
}
