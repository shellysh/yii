<?php

use yii\db\Migration;

class m170527_121826_create_table_students extends Migration
{
    public function up()
    {
		$this->createTable('students',[
				'id'=> $this->primaryKey(),
				'name' => $this->string()->notNull(),
				'age' => $this->integer()->notNull(),
		]);
    }

    public function down()
    {
        $this->dropTable('students');
    }
}
