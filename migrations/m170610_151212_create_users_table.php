<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170610_151212_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
			'username' => $this->string()->notNull(),
			'password' => $this->string()->notNull(),
			'authKey' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
