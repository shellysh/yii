<?php

use yii\db\Migration;

/**
 * Handles the creation of table `movies`.
 */
class m170610_162854_create_movies_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('movies', [
            'key' => $this->primaryKey(),
			'moviename' => $this->string()->notNull(),
			'genre' => $this->string()->notNull(),
			'minage' => $this->integer()->notNull(),
			'score' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('movies');
    }
}
