<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170718_083008_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'username' => $this->string()->notNull(),
			'password' => $this->string()->notNull(),
			'authKey' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
