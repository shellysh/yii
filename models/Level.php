<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "level".
 *
 * @property integer $id
 * @property string $name
 */
class Level extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	//a.g
	//מאפשר לבנות רשימה בטופס
	public static function getLevels()
	{
		$allLevels = self::find()->all();
		$allLevelsArray = ArrayHelper::
					map($allLevels, 'id', 'name');
		return $allLevelsArray;						
	}
	
	//7
	public static function getLevelsWithAllLevels()
	{
		//שימוש בפונקציה שבנינו לקבלת הסטטוסים
		$allLevels = self::getLevels();
		$allLevels[null] = 'All Levels'; //מקום כזה במערך מכיוון שאין לו פילטר
		$allLevels = array_reverse ( $allLevels, true );
		return $allLevels;	
	}
}
