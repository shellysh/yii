<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $name
 */
class Status extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	//a.g
	//מאפשר לבנות רשימה בטופס
	public static function getStatuses()
	{
		$allStatuses = self::find()->all();
		$allStatusesArray = ArrayHelper::
					map($allStatuses, 'id', 'name');
		return $allStatusesArray;						
	}
	
	//7
	public static function getStatusesWithAllStatuses()
	{
		//שימוש בפונקציה שבנינו לקבלת הסטטוסים
		$allStatuses = self::getStatuses();
		$allStatuses[null] = 'All Statuses'; //מקום כזה במערך מכיוון שאין לו פילטר
		$allStatuses = array_reverse ( $allStatuses, true );
		return $allStatuses;	
	}
}
