<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\Level;
use app\models\Status;
use app\models\User;
/**
 * This is the model class for table "breakdown".
 *
 * @property integer $id
 * @property string $title
 * @property integer $levelId
 * @property integer $statusId
 */
class Breakdown extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breakdown';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'levelId'], 'required'],
            [['levelId', 'statusId'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'levelId' => 'Level ID',
            'statusId' => 'Status ID',
        ];
    }
	
	//a.h
	public function beforeSave($insert){		
		$return = parent::beforeSave($insert);
		if ($this->isNewRecord) {
			$this->setAttribute('statusId','1');
		}

		//5.b
		if($this->isAttributeChanged('levelId')){
			if (!\Yii::$app->user->can('updateToUrgent')){
				if($this->getAttribute('levelId') == 3)
					unset($this->levelId);
			}
		}
		
		return $return;	
	}
	
	//7
	public function getLevelItem()
    {
        return $this->hasOne(Level::className(), ['id' => 'levelId']);
    }
	
	//7
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'statusId']);
    }
}
