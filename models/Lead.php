<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Deal;

/**
 * This is the model class for table "lead".
 *
 * @property integer $id
 * @property string $name
 */
class Lead extends \yii\db\ActiveRecord
{
	public $role;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lead';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	//2016 - 3.a
	//יצירת פונקציה לצורך dropdown
	public static function getLeads()
	{
		$allLeads = self::find()->all();
		$allLeadsArray = ArrayHelper::
					map($allLeads, 'id', 'name');
		return $allLeadsArray;						
	}
	
	/*public function getDealItem()
    {
        return hasOne(Deal::className(), ['leadId' => 'id']);
    }*/
	
	public static function getLeadsofdeals()
	{
		//$model = Deal::findAll('leadId');
		//$allLeads = self::findAll(['id' => $model]);
		/*$allLeadsj = self::find()->all();
		$allLeads = Deal::findAll(['leadId' => $allLeadsj]);*/
		$allLeadsj = self::find()->all();
		$allLeads = Deal::findAll(['leadId' => $allLeadsj]);
		$allLeadsArray = ArrayHelper::
					map($allLeads, 'id', 'leadId');
		return $allLeadsArray;						
	}
	
	//2016 - 4.c
	public static function getExistLeadsWithAllLeads(){
		//יצירת אובייקט חדש מסוג דיל
		$deal = new Deal();
		//מציאת כל האידי של הלידים, ללא כפילויות
		$dealExist = $deal->find()->select('leadId')->distinct()->all();
		$dealExistArr = [];
		//הכנסת הערכים לתוך מערך
		foreach($dealExist as $i){
			$dealExistArr[] = $i->leadId;
		}
		//בעזרת השאילתא נמצא את הלידים הקיימים
		$allLeads = self::find()->where(['id' => $dealExistArr])->all();
		$allLeadsArray = ArrayHelper::
					map($allLeads, 'id', 'name');
		
		$allLeadsArray[null] = 'All Leads';
		$allLeadsArray = array_reverse ( $allLeadsArray, true );
		return $allLeadsArray;
	}

	//2016 - 5.a
	public function getDealItem(){
		return $this->hasMany(Deal::className(), ['leadId' => 'id']);
		
	}
}
