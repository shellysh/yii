<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Student extends ActiveRecord{
    public static function tableName(){
		return 'students';
	}
	
	public static function getAge($id){
		$student = self::findOne($id);
		
		isset($student)?
		$return = $student->age: 
		$return = "No Student found with id $id"; 
		return $return;;
	}

	public static function getName($id){
		$student = self::findOne($id);

		isset($student)?
		$return = $student->name: 
		$return = "No Student found with id $id"; 
		return $return;
	}
	public static function getTable(){
		return self::find()->all();
	}

}
