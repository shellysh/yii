<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
	//3.a
	public $role; //משתנה להגדרת תפקידים
	
	 public static function tableName(){
		return 'users';
	}
	
	//הגדרת ולידציה
	public function rules(){
		return 
		[
			[['username','password','authKey'],'string','max' => 255],
			[['username','password'],'required'],
			[['username'],'unique'],
			['role', 'safe'], //3.a
		];
	}

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
		return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
		throw new NotSupportedException('Not supported');
		return null;
        /*foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;*/
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
		return self::findOne(['username' => $username]);
        /*foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;*/
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
		return $this->isCorrectHash($password, $this->password);
        //return $this->password === $password;
    }
	
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
	
	//hash password before saving a new user
    public function beforeSave($insert) 
    {
        $return = parent::beforeSave($insert);
		//בדיקה האם הסיסמה השתנתה - אירוע של עדכון
        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password); //ביצוע קידוד - HASH
		//בודק האם מדובר במשתמש חדש, ומטרתו לעדכן את המפתח
        if ($this->isNewRecord)
		    $this->authKey = Yii::$app->security->generateRandomString(32);

        return $return;
    }
	
	//3.a
	//יצירת מערך להגדרת התפקידים
	public static function getRoles()
	{
		//if(\Yii::$app->user->can('admin', ['user' =>$model])){
			//מכיוון שאין פונקציה היוצרת את התפקידים כפי שאנו רוצים, אז ניצור מערך
			$rolesObjects = Yii::$app->authManager->getRoles();
			$roles = [];
			foreach($rolesObjects as $id =>$rolObj){
				$roles[$id] = $rolObj->name; 
			}
			
			return $roles; 	
		//}
	}
	
	//3.a
	//מאפשר שמירה בטבלת auth_assignment
	public function afterSave($insert,$changedAttributes)
    {
		//3.b
		//מאפשר רק למי שבעל הרשאות אדמין להגדיר rule
		if(\Yii::$app->user->can('admin', ['user' =>$this])){
			$return = parent::afterSave($insert, $changedAttributes);

			$auth = Yii::$app->authManager;
			$roleName = $this->role; 
			$role = $auth->getRole($roleName); //object of the role
			//יש לו אידי מכיוון שאנחנו כבר יצרנו משתמש חדש
			if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
				$auth->assign($role, $this->id);
			} else {
				//ביצוע המחיקה יהיה באמצעות פקודת SQL
				$db = \Yii::$app->db;
				$db->createCommand()->delete('auth_assignment',
					['user_id' => $this->id])->execute();
				$auth->assign($role, $this->id);
			}

			return $return;
		}
    }
}
