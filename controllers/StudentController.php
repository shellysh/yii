<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Student;

class StudentController extends Controller
{
	public function actionView($id)
    {
		$id = $id;
		$name = Student::getName($id);
		$age = Student::getAge($id);
        return $this->render('view',['id'=> $id , 'name' => $name, 'age' => $age]);
    }
	
	public function actionAll(){
		$table = Student::getTable();
		return $this->render('all',['table' => $table]);
	}
}